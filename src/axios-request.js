import axios from 'axios'

const axiosPages = axios.create({
    baseURL: 'https://info-js-default-rtdb.firebaseio.com/'
});

export default axiosPages;
