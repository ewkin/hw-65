import React from 'react';
import NavigationItem from "./NavigationItem/NavigationItem";
import './Navigation.css';
import {URLS} from "../../constants";

const Navigation = () => {
    return (
        <header className="Navigation">
            <div className="Navigation-logo">Homework 65</div>
            <nav>
                <ul className="NavigationItems">
                    <NavigationItem to='/' exact>Home</NavigationItem>
                    {URLS.map(key => (
                        <NavigationItem key={key} to={'/pages/'+key} exact>{key.charAt(0).toUpperCase() + key.slice(1)}</NavigationItem>
                    ))}
                </ul>
            </nav>
        </header>
    );
};

export default Navigation;