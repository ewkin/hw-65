import React from 'react';

import './Layout.css';
import Navigation from "../Navigation/Navigation";

const Layout = props => {

    return (
        <>
            <Navigation/>
            <main className="Layout-Content" >
                {props.children}
            </main>
        </>
    );
};

export default Layout;