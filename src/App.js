import React from 'react';
import {Switch, Route} from "react-router-dom";
import Layout from "./Components/Layout/Layout";
import Page from "./Containers/Page/Page";


const App = () =>
    (
        <Layout>
            <Switch>
                <Route path="/" exact component={Page}/>
                <Route path="/pages/:id" exact component={Page}/>
                <Route render={() => <h1>Not Found</h1>}/>
            </Switch>
        </Layout>
    );

export default App;