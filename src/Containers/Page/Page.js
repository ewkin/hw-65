import React, {useEffect, useState} from 'react';
import axiosPages from "../../axios-request";
import {URLS} from "../../constants";

const Page = props => {
    const [page, setPage] = useState({
        page: 'home',
        title: '',
        content: '',
    })

    let url = 'home';
    let admin = false;

    if (URLS.includes(props.match.params.id)) {
        url = props.match.params.id;
        admin = props.match.params.id === URLS[URLS.length - 1];
    }


    useEffect(() => {
        if (!admin) {
            const fetchData = async () => {
                const response = await axiosPages.get(url + '.json');
                setPage(prevState => ({
                    ...prevState,
                    page: url, title: response.data.title, content: response.data.content,
                }));
            };
            fetchData().catch(error => {
                console.log(error)
            });
        }
    }, [url, admin]);

    const getPageInput = event => {
        const {name, value} = event.target;
        setPage(prevState => ({
            ...prevState,
            [name]: value,
        }));
    };
    const getPageInputRequest = async event => {
        const {name, value} = event.target;
        const response = await axiosPages.get(event.target.value + '.json');
        setPage(prevState => ({
            ...prevState,
            [name]: value, title: response.data.title, content: response.data.content,
        }));
    };

    const editPage = async event => {
        event.preventDefault();
        try {
            await axiosPages.put(page.page + '.json', {title: page.title, content: page.content});
        } catch (e) {
            console.log(e);
        } finally {
            if (page.title === 'home') {
                props.history.replace();
            }
            props.history.replace('/pages/' + page.page);
        }
    };


    let pageLayout = (
        <div>
            <h1>{page.title}</h1>
            <p>{page.content}</p>
        </div>
    );

    if (admin) {
        pageLayout = (
            <div>
                <h1>Edit Pages</h1>
                <form onSubmit={editPage}>
                    <div>
                        <label>
                            Select Page:
                            <select name="page" value={page.page} onChange={getPageInputRequest}>
                                {['home', ...URLS].slice(0, -1).map((url) => (
                                    <option key={url} value={url}>{url.charAt(0).toUpperCase() + url.slice(1)}</option>
                                ))}
                            </select>
                        </label>
                    </div>
                    <div>
                        <label>
                            Title:
                            <input
                                required placeholder="Title"
                                type="text" name="title"
                                value={page.title} onChange={getPageInput}
                            />
                        </label>
                    </div>
                    <div>
                        <label>
                            Content:
                            <textarea
                                required placeholder="Content"
                                name="content"
                                value={page.content} onChange={getPageInput}
                            />
                        </label>
                    </div>
                    <button type="submit" className="Button">Save</button>
                </form>
            </div>
        )
    }

    return pageLayout;
};

export default Page;